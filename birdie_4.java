//package assignment1;
//import processing.core.PApplet;
//import java.util.*;
//
//public class birdie {
//
//	double x, z;
//	double vx, vz, vr;
//	double r;
//	
//	double vMax,vMaxSq, rMax, aMax, aMaxSq;
//	// steering output
//	double ax, az;
//	double ar;
//	PApplet canvas;
//	
//	final double pi = Math.PI;
//	final int birdCount = 7;
//	double targetX, targetZ;
//	boolean wander;
//	
//	final double radiusSat = 20, radiusDeccl = 400;
//	final double timeToTargetVelocity = 500;
//	double neighbours[][] = new double[10][2];
//	List trace = new ArrayList();
//	
//	int counter, id;
//	int traceLength;
//	long time,time1,time2;
//	double target[] = new double[2];
//	
//	birdie(PApplet p,int i,boolean w,double xp,double zp,double v, double d)
//	{
//		id = i;
//		wander = w;
//		time2 = time1 = time = System.currentTimeMillis();
//		canvas = p;
//		x = xp;
//		z = zp;
//		vx = 0.00;
//		vz = 0.00;
//		vr = 0.00;
//		r = 0;
//		vMax = v;
//		vMaxSq = Math.sqrt(vMax);
//		rMax = d;
//		aMax = 0.002;
//		aMaxSq = Math.sqrt(aMax);
//		target[0] = Math.round(Math.random()*600) + 50;
//		target[1] = Math.round(Math.random()*600) + 50;
//		
////									rMax = 0.5;
////									arMax = 0.01;
//		counter = 0;
//		traceLength = 24;
//		
//	}
//	public void shadowPrint(int xC, int zC,double rT,int color)
//	{
//		int x1,x2,x3,z1,z2,z3;
//		double cos = Math.cos(rT);
//		double sine = Math.sin(rT);		
//		
//		x1 = (int)Math.round(4.0*cos - 5.0*sine);
//		z1 = (int)Math.round(4.0*sine + 5.0*cos);
//		x2 = (int)Math.round(4.0*cos + 5.0*sine);
//		z2 = (int)Math.round(4.0*sine - 5.0*cos);
//		x3 = (int)Math.round(12.5*cos);
//		z3 = (int)Math.round(12.5*sine);
//		
//		if(id==0 || id == 1)
//			canvas.fill(255,color,color);
//		else
//			canvas.fill(color);
//		canvas.ellipse(xC,zC,13,13);
//		canvas.triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
//	}
//	
//	public void shapePrint()
//	{
//		int xC, zC;
//		int x1,x2,x3,z1,z2,z3;
//		double cos = Math.cos(r);
//		double sine = Math.sin(r);		
//				
//		xC = (int)x;
//		zC = (int)z;
//		x1 = (int)Math.round(8.0*cos - 10.0*sine);
//		z1 = (int)Math.round(8.0*sine + 10.0*cos);
//		x2 = (int)Math.round(8.0*cos + 10.0*sine);
//		z2 = (int)Math.round(8.0*sine - 10.0*cos);
//		x3 = (int)Math.round(25.0*cos);
//		z3 = (int)Math.round(25.0*sine);
//		
//		if(id==0|| id == 1)
//			canvas.fill(255,0,0);
//		else
//			canvas.fill(0);
//		canvas.ellipse(xC,zC,25,25);
//		canvas.triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
//	}
//	
//	public void shadow()
//	{
//		int length = trace.size();
//		for (int i = 0; i < length; i+=3) {
//			int xC = (int)Math.round((double)trace.get(i));
//			int zC = (int)Math.round((double)trace.get(i+1));
//			double or = (double)trace.get(i+2);
//			shadowPrint(xC,zC,or,180*(length-i)/length);
//		}
//	}
//	
//	public void setNewOrientation2()
//	{
//		double r2, rot=0;
//		if((vz*vz + vx*vx) > 0)
//		{
//			r2 = Math.atan2(vz,vx);
//			rot = r2 - r;
//			
//			if(rot > pi)
//				rot -= 2*pi;
//			else if(rot < -pi)
//				rot += 2*pi;
//			
//			if(Math.abs(rot)<rMax)
//			{
//				r = r2;
//				rot = 0;
//			}
//			else
//			{
//				if( Math.abs(rot) > rMax)
//					rot =  (rot > 0)? rMax : -rMax;
//				r += rot;
//			}
//		}
//	}
//	
//	public void setNewOrientation()
//	{
//		double rand = Math.random() - Math.random() + Math.random() - Math.random() ;
//		//vr = (Math.random() * rMax * 2) - rMax;
//		vr = rand * rMax / 2;
//		r += vr;
//		r = r % (2*pi);
//	}
//	
//	public void seperation()
//	{
//		double decayCoefficient = 10;
//		int threshold = 50;
//		double acx = 0, acz = 0, dist;
//		double strength;
//		
//		for(int i=0;i<birdCount;i++)
//		{
//			if(i==id)	continue;
//			dist = euclidean(neighbours[i]);
//			if(dist == 0)
//			{
//				ax += aMax;
//				az += aMax;
//			}
//			if(dist < threshold)
//			{
//				strength = Math.min(decayCoefficient/(dist*dist),aMax);
//				acx += (neighbours[i][0]-x)*strength*2;
//				acz += (neighbours[i][1]-z)*strength*2;
//			}
//		}
//		if(acx == 0  && acz == 0) 
//		{
//			ax = 0;
//			az = 0;
//			return;
//		}
//		dist = Math.sqrt(acx*acx + acz*acz);
//		if(dist > aMax)
//		{
//			ax = acx/dist*aMaxSq;
//			az = acz/dist*aMaxSq;
//		}
//		
//	}
//	
//	public void update2()
//	{
//		setNewOrientation2();
//		
//		long newTime = System.currentTimeMillis();
//		long diff = newTime - time;
//		
//		
//		seperation();
//		
//		vx += ax*diff;
//		vz += az*diff;
//		
//		
//		x += vx*diff - 0.5*ax*diff*diff;
//		z += vz*diff - 0.5*az*diff*diff;
//		r += vr*diff;
//		
//		
//		
//		
//		time = newTime;
//		newTime = System.currentTimeMillis();
//		if(newTime - time2 > 350 )
//		{
//			updateTraceList();
//			time2 = newTime;
//		}
//	}
//	
//	public void wander()
//	{
//		/* Methond 1
//		x += 10 * vMaxSq * Math.cos(r);
//		z += 10 * vMaxSq * Math.sin(r);
//		
//		if(x > 650) x = 650;
//		if(x < 50) x = 50;
//		if(z > 650) z = 650;
//		if(z < 50) z = 50;
//		
//		if((r <= 2*pi) && (x >= 650 || x <= 50 || z >= 650 || z <= 50) )
//		{
//			r += pi/16;
//		}
//		if(counter == 0)
//			setNewOrientation();
//		counter = (counter++)%8;
//		*/
//		
//		long newTime = System.currentTimeMillis();
//		if(newTime - time1 > 2000)
//		{
//			double xc,zc,theta;
////			do
////			{
////				theta = (Math.random() - Math.random())*pi/2; 
////				xc = 70 * Math.cos(r+theta);
////				zc = 70 * Math.sin(r+theta);
////				System.out.println(x+" "+z+" "+xc+" "+zc+" "+r);
////			}while(!((x+xc) > 50 && (x+xc)<650 && (z+zc) > 50 && (z+zc) < 650));	
//			
//			xc = Math.random()*600 + 50;
//			zc = Math.random()*600 + 50;
//			
//			target[0] = xc;
//			target[1] = zc;
//			time1 = newTime;
//		}
//		
//		seek(target);
//		if(newTime - time2 > 350 )
//		{
//			updateTraceList();
//			time2 = newTime;
//		}
//	}
//	
//	public void seek(double t[])
//	{
//		target[0] = t[0];
//		target[1] = t[1];
//		double dx = t[0] - x;
//		double dz = t[1] - z;
//		double dist = Math.sqrt((dx*dx) + (dz*dz));
//		double goalSpeed, velX, velZ;
//		
//		if(dist < radiusSat)
//		{
//			ax = -vx/timeToTargetVelocity;
//			az = -vz/timeToTargetVelocity;
//			update2();
//			
//			return;
//		}
//		if(dist > radiusDeccl)
//		{
//			goalSpeed = vMax;
//		}
//		else
//		{
//			goalSpeed = vMax*dist/radiusDeccl;
//		}
//		
//		velX = dx/Math.sqrt(dx*dx + dz*dz)*Math.sqrt(goalSpeed); 
//		velZ = dz/Math.sqrt(dx*dx + dz*dz)*Math.sqrt(goalSpeed);
//		
//		ax = (velX - vx)/timeToTargetVelocity;
//		az = (velZ - vz)/timeToTargetVelocity;
//		
//		update2();
//	}
//	
//	
//	public double magnitude(double a, double b)
//	{
//		return Math.sqrt(a*a + b*b);
//	}
//	public double euclidean(double l[])
//	{
//		return Math.sqrt((l[0]-x)*(l[0]-x) + (l[1]-z)*(l[1]-z));
//	}
//	public double[] move(double t[][])
//	{
//		neighbours = t;
//		if(wander)
//			wander();
//		else
//		{
//			double dist1, dist2;
//			
//			dist1 = euclidean(t[0]);
//			dist2 = euclidean(t[1]);
//			
//			seek(t[dist1>dist2?1:0]);
//		}
//			
//		
//		double Loc[] = new double[2];
//		Loc[0] = x;
//		Loc[1] = z;
//		return Loc;
//	}
//	
//	public void printvalues()
//	{
//		System.out.println("x: "+x+" z: "+z+"vx: "+vx+" vz: "+vz+" tX: "+targetX+" tZ: "+targetZ);
//	}
//	
//	public void display()
//	{
//		shadow();
//		shapePrint();
//	}
//	public void updateTraceList()
//	{
//		if(trace.size() > traceLength)
//		{
//			trace.remove(0);
//			trace.remove(0);
//			trace.remove(0);
//		}
//		trace.add(x);
//		trace.add(z);
//		trace.add(r);
//	}
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//
//	}
//
//}



package assignment1;
import processing.core.PApplet;
import java.util.*;

public class birdie {

	double x, z;
	double vx, vz, vr;
	double r;
	
	double vMax,vMaxSq, rMax, aMax, aMaxSq;
	// steering output
	double ax, az;
	double ar;
	PApplet canvas;
	
	final double pi = Math.PI;
	final int birdCount = 8;
	double targetX, targetZ;
	boolean wander;
	
	final double radiusSat = 20, radiusDeccl = 400;
	final double timeToTargetVelocity = 500;
	double neighbours[][] = new double[10][2];
	List trace = new ArrayList();
	
	int counter, id;
	int traceLength;
	long time,time1,time2;
	double target[] = new double[2];
	
	birdie(PApplet p,int i,boolean w,double xp,double zp,double v, double d)
	{
		id = i;
		wander = w;
		time2 = time1 = time = System.currentTimeMillis();
		canvas = p;
		x = xp;
		z = zp;
		vx = 0.00;
		vz = 0.00;
		vr = 0.00;
		r = 0;
		vMax = v;
		vMaxSq = Math.sqrt(vMax);
		rMax = d;
		aMax = 0.002;
		aMaxSq = Math.sqrt(aMax);
		target[0] = Math.round(Math.random()*600) + 50;
		target[1] = Math.round(Math.random()*600) + 50;
		
//									rMax = 0.5;
//									arMax = 0.01;
		counter = 0;
		traceLength = 24;
		
		
		
	}
	
	public void shadowPrint(int xC, int zC,double rT,int color)
	{
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(rT);
		double sine = Math.sin(rT);		
		
		x1 = (int)Math.round(4.0*cos - 5.0*sine);
		z1 = (int)Math.round(4.0*sine + 5.0*cos);
		x2 = (int)Math.round(4.0*cos + 5.0*sine);
		z2 = (int)Math.round(4.0*sine - 5.0*cos);
		x3 = (int)Math.round(12.5*cos);
		z3 = (int)Math.round(12.5*sine);
		
		if(id==0 || id == 1)
			canvas.fill(255,color,color);
		else
			canvas.fill(color);
		canvas.ellipse(xC,zC,13,13);
		canvas.triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	
	public void shapePrint()
	{
		int xC, zC;
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(r);
		double sine = Math.sin(r);		
				
		xC = (int)x;
		zC = (int)z;
		x1 = (int)Math.round(8.0*cos - 10.0*sine);
		z1 = (int)Math.round(8.0*sine + 10.0*cos);
		x2 = (int)Math.round(8.0*cos + 10.0*sine);
		z2 = (int)Math.round(8.0*sine - 10.0*cos);
		x3 = (int)Math.round(25.0*cos);
		z3 = (int)Math.round(25.0*sine);
		
		if(id==0|| id == 1)
			canvas.fill(255,0,0);
		else
			canvas.fill(0);
		canvas.ellipse(xC,zC,25,25);
		canvas.triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	
	public void shadow()
	{
		int length = trace.size();
		for (int i = 0; i < length; i+=3) {
			int xC = (int)Math.round((double)trace.get(i));
			int zC = (int)Math.round((double)trace.get(i+1));
			double or = (double)trace.get(i+2);
			shadowPrint(xC,zC,or,180*(length-i)/length);
		}
	}
	
	public void setNewOrientation2()
	{
		double r2, rot=0;
		if((vz*vz + vx*vx) > 0)
		{
			r2 = Math.atan2(vz,vx);
			rot = r2 - r;
			
			if(rot > pi)
				rot -= 2*pi;
			else if(rot < -pi)
				rot += 2*pi;
			
			if(Math.abs(rot)<rMax)
			{
				r = r2;
				rot = 0;
			}
			else
			{
				if( Math.abs(rot) > rMax)
					rot =  (rot > 0)? rMax : -rMax;
				r += rot;
			}
		}
	}
	
	public void setNewOrientation()
	{
		double rand = Math.random() - Math.random();
		//vr = (Math.random() * rMax * 2) - rMax;
		vr = rand * rMax;
		r += vr;
		r = r % (2*pi);
	}
	
	public void seperation()
	{
		double decayCoefficient = 10;
		int threshold;
		double acx = 0, acz = 0, dist;
		double strength;
		
		for(int i=0;i<birdCount;i++)
		{
			if(i==id)	continue;
			if(i == 0 || i == 1)
				threshold = 50;
			else
				threshold = 40;
			
			dist = euclidean(neighbours[i]);
			if(dist == 0)
			{
				ax += aMax;
				az += aMax;
			}
			if(dist < threshold)
			{
				strength = Math.min(decayCoefficient/(dist*dist),aMax);
				acx += (neighbours[i][0]-x)*strength*2;
				acz += (neighbours[i][1]-z)*strength*2;
			}
		}
		if(acx == 0  && acz == 0) 
		{
			return;
		}
		ax += acx/(birdCount-1);
		az += acz/(birdCount-1);
	}
	
	public void update()
	{
		
		long newTime = System.currentTimeMillis();
		long diff = newTime - time;
		double magnitude;
		
		
		magnitude = Math.sqrt(ax*ax + az*az);
		
		if(magnitude > aMax)
		{
			ax = ax/magnitude * aMaxSq;
			az = az/magnitude * aMaxSq;
		}
		
		vx += ax*diff;
		vz += az*diff;
		
		seperation();
		
		x += (vx*diff - 0.5*ax*diff*diff)%4;
		z += (vz*diff - 0.5*az*diff*diff)%4;
		r += vr*diff;
		
		time = newTime;
		newTime = System.currentTimeMillis();
		if(newTime - time2 > 350 )
		{
			updateTraceList();
			time2 = newTime;
		}
		
		setNewOrientation2();
		
	}
	
	public void wander()
	{
		long newTime = System.currentTimeMillis();
		if(newTime - time1 > 2500)
		{
			do
			{
				target[0] = Math.round(Math.random()*600) + 50;
				target[1] = Math.round(Math.random()*600) + 50;
			}while(euclidean(target) < 50);
			time1 = newTime;
		}
		
		seek(target);
		newTime = System.currentTimeMillis();
		
		if(newTime - time2 > 350 )
		{
			updateTraceList();
			time2 = newTime;
		}
	}
	
	public void seek(double t[])
	{
		target[0] = t[0];
		target[1] = t[1];
		double dx = t[0] - x;
		double dz = t[1] - z;
		double dist = Math.sqrt((dx*dx) + (dz*dz));
		double goalSpeed, velX, velZ;
		
		if(dist < radiusSat)
		{
			ax = -vx/timeToTargetVelocity;
			az = -vz/timeToTargetVelocity;
			update();
			
			return;
		}
		if(dist > radiusDeccl)
		{
			goalSpeed = vMax;
		}
		else
		{
			goalSpeed = vMax*dist/radiusDeccl;
		}
		
		velX = dx/Math.sqrt(dx*dx + dz*dz)*Math.sqrt(goalSpeed); 
		velZ = dz/Math.sqrt(dx*dx + dz*dz)*Math.sqrt(goalSpeed);
		
		ax = (velX - vx)/timeToTargetVelocity;
		az = (velZ - vz)/timeToTargetVelocity;
		
		update();
		
		if(x < 0 || z < 0 || x > 700 || z > 700)
		{
			System.out.println("*************************************************");
			System.out.println(id+" "+x+" "+z+" "+vx+" "+vz+" "+ax+" "+az);
		}
		
	}
	
	public double euclidean(double l[])
	{
		return Math.sqrt((l[0]-x)*(l[0]-x) + (l[1]-z)*(l[1]-z));
	}

	public double[] move(double t[][])
	{
		neighbours = t;
		if(wander)
			wander();
		else
		{
			double dist1, dist2;
			
			dist1 = euclidean(t[0]);
			dist2 = euclidean(t[1]);
			
			seek(t[dist1>dist2?1:0]);
		}
			
		if( x > 650) ax = -aMaxSq;
		if( x > 675) ax = -aMax;
		if( x < 50 ) ax = aMaxSq;
		if( x < 25 ) ax = aMax;
		if( z > 650 ) az = -aMaxSq;
		if( z > 675 ) az = -aMax;
		if( z < 50 ) az = aMaxSq;
		if( z < 25 ) az = aMax;
		
		double Loc[] = new double[2];
		Loc[0] = x;
		Loc[1] = z;
		return Loc;
	}
	
	public boolean isOutOfRange()
	{
//		if(x>650 || x < 50 || z > 650 || z < 50)
//		{
//			System.out.println(id+" "+x+" "+z);
//			return true;
//		}
		return false;
	}

	public void printvalues()
	{
		System.out.println("x: "+x+" z: "+z+"vx: "+vx+" vz: "+vz+" tX: "+targetX+" tZ: "+targetZ);
	}
	
	public void display(int x)
	{
		if(x==0)
			shadow();
		else
			shapePrint();
	}

	public void updateTraceList()
	{
		if(trace.size() > traceLength)
		{
			trace.remove(0);
			trace.remove(0);
			trace.remove(0);
		}
		trace.add(x);
		trace.add(z);
		trace.add(r);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

