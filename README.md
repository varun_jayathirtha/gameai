# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Quick summary ###

1.  This is a small project to get familiar with GAME AI principles.
2.  Fixed movement, wandering, seeking, flocking are shown using different examples

1. Basic_motion.jar - Basic Motion

2. Seek_steering1.jar - Seek clicked point with gradual acceleration and deceleration.

3. Seek_steering2.jar - Seek clicked point with gradual acceleration but instantaneous * deceleration.

4. Wander_steering1.jar - Random rotation followed by, motion in fowrard direction.

5. Wander_steering2.jar - Random selection of a goal point, seeking the chosen goal point till the next random goal is chosen.

6. Flocking_behavior1.jar - 1 leader, 2 followers

7. Flocking_behavior2.jar - 1 leader, 4 followers

8. Flocking_behavior3.jar - 2 leaders, 5 followers

### Summary of set up ###

1. JAR files have the dependencies included in them. Nothing more required. Execute the JARs directly.
2. The Project folders are directly extracted from Eclipse Project with support for 'Processing'. 

[This](https://processing.org/tutorials/eclipse/) gives information about setting up 'Processing' on Eclipse.

### Repo owner or admin ###

Varun Jayathirtha

vjayath@ncsu.edu