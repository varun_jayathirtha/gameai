package assignment1;

import processing.core.PApplet;
import java.util.*;

public class part4 extends PApplet {
	int xMax, zMax;
	final int birdCount = 8;
	birdie bird[] = new birdie[birdCount];
	int counter;
	boolean first = true;
	
	
	double loc[][] = new double[10][2];  
	public void setup() {
	}
	
	public void settings() {
		
		for(int i=0;i<birdCount;i++)
		{
			for(int j=0;j<2;j++)
				loc[i][j] = Math.round(Math.random()*600) + 50;
			
			for(int j=0;j<i;j++)
			{
				if(loc[i][0]-loc[j][0] < 30 && loc[i][1]-loc[j][1] < 30)
				{
					i--;
					break;
				}
			}
		}
		bird[0] = new birdie(this,1,true,loc[0][0],loc[0][1],0.025,0.04);
		bird[1] = new birdie(this,1,true,loc[1][0],loc[1][1],0.025,0.04);
		
		for(int i=2;i<birdCount;i++)
			bird[i] = new birdie(this,i,false,loc[i][0],loc[i][1],0.02,0.025);
		
		xMax = 700;
		zMax = 700;
		
		size(xMax,zMax);
	}
	
	public void draw() {
	
		noStroke();
		background(255);
		
		for(int i=0;i<birdCount;i++)
			bird[i].display(0);
		for(int i=0;i<birdCount;i++)
			bird[i].display(1);
		
		if(first)
		{
			try {
			    Thread.sleep(1000);                 //1000 milliseconds is one second.
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
			first = false;
		}
		update();
	}
	
	public void update()
	{
		for(int i=0;i<birdCount;i++)
			loc[i] = bird[i].move(loc);
	}	
	
	public static void main(String _args[]) {
		PApplet.main(new String[] { assignment1.part4.class.getName() });
	}
}


