package assignment1;

import processing.core.PApplet;
import java.util.*;

public class part2 extends PApplet {
	int xMax, zMax;
	
	// Kinematic 
	double x, z;
	double vx, vz, vr;
	double r;
	
	double vMax,vMaxSq, rMax;
	// steering output
	double ax, az;
	double ar;
	
	final double pi = Math.PI;
	
	double targetX, targetZ;
	boolean started;
	
	final double timeToTargetVelocity = 500;
	
	int counter,target;
	int traceLength;
	long time;
	
	List trace = new ArrayList();
	  
	public void setup() {
	}
	public void settings() {
		started = false;
		targetX = 0;
		targetZ = 0;
		xMax = 700;
		zMax = 700;
		x = 350;
		z = 350;
		vx = 0.00;
		vz = 0.00;
		vr = 0.00;
		r = 0;
		
		vMax = 0.02;
		vMaxSq = Math.sqrt(vMax);
		rMax = 0.025;
		counter = 0;
		traceLength = 50;
		time = System.currentTimeMillis();
	    size(xMax,zMax);
	}
	public void shapePrint()
	{
		int xC, zC;
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(r);
		double sine = Math.sin(r);		
				
		xC = (int)x;
		zC = (int)z;
		x1 = (int)Math.round(8.0*cos - 10.0*sine);
		z1 = (int)Math.round(8.0*sine + 10.0*cos);
		x2 = (int)Math.round(8.0*cos + 10.0*sine);
		z2 = (int)Math.round(8.0*sine - 10.0*cos);
		x3 = (int)Math.round(25.0*cos);
		z3 = (int)Math.round(25.0*sine);
		
		fill(0);
		ellipse(xC,zC,25,25);
		triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	public void shadowPrint(int xC, int zC,double rT,int color)
	{
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(rT);
		double sine = Math.sin(rT);		
		
		x1 = (int)Math.round(4.0*cos - 5.0*sine);
		z1 = (int)Math.round(4.0*sine + 5.0*cos);
		x2 = (int)Math.round(4.0*cos + 5.0*sine);
		z2 = (int)Math.round(4.0*sine - 5.0*cos);
		x3 = (int)Math.round(12.5*cos);
		z3 = (int)Math.round(12.5*sine);
		
		fill(color);
		ellipse(xC,zC,13,13);
		triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	public void shadow()
	{
		int length = trace.size();
		for (int i = 0; i < length; i+=3) {
			int xC = (int)Math.round((double)trace.get(i));
			int zC = (int)Math.round((double)trace.get(i+1));
			double or = (double)trace.get(i+2);
			shadowPrint(xC,zC,or,180*(length-i)/length);
		}
	}
	
	public void setNewOrientation()
	{
		double r2, rot=0;
		if((vz*vz + vx*vx) > 0)
		{
			r2 = Math.atan2(vz,vx);
			rot = r2 - r;
			
			if(rot > pi)
				rot -= 2*pi;
			else if(rot < -pi)
				rot += 2*pi;
			
			if(Math.abs(rot)<rMax)
			{
				r = r2;
				rot = 0;
			}
			else
			{
				if( Math.abs(rot) > rMax)
					rot =  (rot > 0)? rMax : -rMax;
				r += rot;
			}
		}
	}
	
	public void seek(double tX, double tZ)
	{
		double dx = tX - x;
		double dz = tZ - z;
		double dist = Math.sqrt((dx*dx) + (dz*dz));
		double velX, velZ;
		
		if(dist < 4)
		{
			setNewOrientation();
			vz = vx = az = ax = 0;
		}
		else
		{
			velX = dx/Math.sqrt(dx*dx + dz*dz)*vMaxSq; 
			velZ = dz/Math.sqrt(dx*dx + dz*dz)*vMaxSq;
			
			ax = (velX - vx)/timeToTargetVelocity;
			az = (velZ - vz)/timeToTargetVelocity;
			
			setNewOrientation();
		
			setNewOrientation();
		}
	}
	
	public void draw() {
		  noStroke();
		  background(255);
		  
		  shadow();
		  shapePrint();
		  
		 update();
	}
	
	public void update()
	{
		long newTime = System.currentTimeMillis();
		long diff = newTime - time;
		
		if(started)
		{
			seek(targetX,targetZ);
		
			x += vx*diff + 0.5*ax*diff*diff;
			z += vz*diff + 0.5*az*diff*diff;
			r += vr*diff + 0.5*ar*diff*diff;
			
			vx += ax*diff;
			vz += az*diff;
			vr += ar*diff;
		}	
		if(counter == 0)
		{
			if(trace.size() > traceLength)
			{
				trace.remove(0);
				trace.remove(0);
				trace.remove(0);
			}
			trace.add(x);
			trace.add(z);
			trace.add(r);
		}
		counter = (counter+1) % 20;
		time = newTime;
	}	
	
	public void mousePressed()
	{
		targetX = mouseX;
		targetZ = mouseY;
		started = true;
	}
	public static void main(String _args[]) {
		PApplet.main(new String[] { assignment1.part2.class.getName() });
	}
}


