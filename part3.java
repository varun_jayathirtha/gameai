package assignment1;

import processing.core.PApplet;
import java.util.*;

public class part3 extends PApplet {
	int xMax, zMax;
	
	birdie bird[] = new birdie[1];
	double clickX, clickZ;
	boolean click;
	int counter;
	  
	public void setup() {
	}
	public void settings() {
		bird[0] = new birdie(this,275,275,0.1,0.25);
		xMax = 700;
		zMax = 700;
		clickX = xMax/2;
		clickZ = zMax/2;
		size(xMax,zMax);
		click = false;
	}
	
	public void draw() {
	
		noStroke();
		background(255);
		  
		bird[0].display();
		  
		update();
	}
	
	public void update()
	{
		long newTime = System.currentTimeMillis();
		double t[] = new double[2];
		bird[0].wander();
		
	}	
	public void mousePressed()
	{
		clickX = mouseX;
		clickZ = mouseY;
		click = true;
	}
	
	public static void main(String _args[]) {
		PApplet.main(new String[] { assignment1.part3.class.getName() });
	}
}


