package assignment1;
import processing.core.PApplet;
import java.util.*;

public class birdie {

	double x, z;
	double vx, vz, vr;
	double r;
	
	double vMax,vMaxSq, rMax, arMax;
	// steering output
	double ax, az;
	double ar;
	PApplet canvas;
	
	final double pi = Math.PI;
	
	double targetX, targetZ;
	boolean started;
	
	final double radiusSat = 20, radiusDeccl = 400;
	final double timeToTargetVelocity = 500;
	
	List trace = new ArrayList();
	
	int counter,target;
	int traceLength;
	long time, time2;
	
	birdie(PApplet p,double a,double b,double c, double d)
	{
		started = false;
		time2 = time = System.currentTimeMillis();
		canvas = p;
		x = a;
		z = b;
		vx = 0.00;
		vz = 0.00;
		vr = 0.00;
		r = 0;
		vMax = c;
		vMaxSq = Math.sqrt(vMax);
		rMax = d;
		arMax = 0.01;
		counter = 0;
		traceLength = 45;
		
	}
	public void shadowPrint(int xC, int zC,double rT,int color)
	{
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(rT);
		double sine = Math.sin(rT);		
		
		x1 = (int)Math.round(4.0*cos - 5.0*sine);
		z1 = (int)Math.round(4.0*sine + 5.0*cos);
		x2 = (int)Math.round(4.0*cos + 5.0*sine);
		z2 = (int)Math.round(4.0*sine - 5.0*cos);
		x3 = (int)Math.round(12.5*cos);
		z3 = (int)Math.round(12.5*sine);
		
		canvas.fill(color);
		canvas.ellipse(xC,zC,13,13);
		canvas.triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	
	public void shapePrint()
	{
		int xC, zC;
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(r);
		double sine = Math.sin(r);		
				
		xC = (int)x;
		zC = (int)z;
		x1 = (int)Math.round(8.0*cos - 10.0*sine);
		z1 = (int)Math.round(8.0*sine + 10.0*cos);
		x2 = (int)Math.round(8.0*cos + 10.0*sine);
		z2 = (int)Math.round(8.0*sine - 10.0*cos);
		x3 = (int)Math.round(25.0*cos);
		z3 = (int)Math.round(25.0*sine);
		
		canvas.fill(0);
		canvas.ellipse(xC,zC,25,25);
		canvas.triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	
	public void shadow()
	{
		int length = trace.size();
		for (int i = 0; i < length; i+=3) {
			int xC = (int)Math.round((double)trace.get(i));
			int zC = (int)Math.round((double)trace.get(i+1));
			double or = (double)trace.get(i+2);
			shadowPrint(xC,zC,or,180*(length-i)/length);
		}
	}
	
	public void setNewOrientation()
	{
		double t = System.currentTimeMillis();
		double rand = Math.random() - Math.random()  ;
		
		if(rand < 0)
			vr = Math.max((rand * rMax ),-rMax) ;
		else
			vr = Math.min((rand * rMax ),rMax) ;
		
		r = (r + vr) % (2*pi);
	}
	
	
	public void wander()
	{
		boolean entered = false;
		double entryAngle = 0;
		x += 5 * vMaxSq * Math.cos(r);
		z += 5 * vMaxSq * Math.sin(r);
		
		if(x > 650) x = 650;
		if(x < 50) x = 50;
		if(z > 650) z = 650;
		if(z < 50) z = 50;
		
		if((r <= 2*pi) && (x >= 650 || x <= 50 || z >= 650 || z <= 50) )
		{
			r += pi/16;
		}
		if(counter == 0)
			setNewOrientation();
		counter = (counter++)%800;
		
		long newTime = System.currentTimeMillis();
		if(newTime - time2 > 350 )
		{
			updateTraceList();
			time2 = newTime;
		}
	}
	
	public double[]	getLoc()
	{
		double Loc[] = new double[2];
		Loc[0] = x;
		Loc[1] = z;
		return Loc;
	}
	public void display()
	{
		shadow();
		shapePrint();
	}
	public void updateTraceList()
	{
		if(trace.size() > traceLength)
		{
			trace.remove(0);
			trace.remove(0);
			trace.remove(0);
		}
		trace.add(x);
		trace.add(z);
		trace.add(r);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
