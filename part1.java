package assignment1;

import processing.core.PApplet;
import java.util.*;

public class Assignment1 extends PApplet {
	int xMax, zMax;
	
	// Kinematic 
	double x, z;
	double vx, vz, vr;
	double r;
	
	double vMax, rMax;
	// steering output
	
	final double pi = Math.PI;
	
	double tList[][];
	
	int counter,target;
	int traceLength;
	long time;
	
	List trace = new ArrayList();
	  
	public void setup() {
	}

	public void settings() {
		tList = new double[4][2];
		tList[0][0] = 100;
		tList[0][1] = 100;
		tList[1][0] = 600;
		tList[1][1] = 100;
		tList[2][0] = 600;
		tList[2][1] = 600;
		tList[3][0] = 100;
		tList[3][1] = 600;
		target = 0;
		xMax = 700;
		zMax = 700;
		x = 350;
		z = 350;
		vx = 0.00;
		vz = 0.00;
		vr = 0.005;
		r = (pi)/4;
		
		vMax = 0.02;
		rMax = 0.05;
		counter = 0;
		traceLength = 50;
		time = System.currentTimeMillis();
	    size(xMax,zMax);
	}
	
	public void shapePrint()
	{
		int xC, zC;
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(r);
		double sine = Math.sin(r);		
				
		xC = (int)x;
		zC = (int)z;
		x1 = (int)Math.round(8.0*cos - 10.0*sine);
		z1 = (int)Math.round(8.0*sine + 10.0*cos);
		x2 = (int)Math.round(8.0*cos + 10.0*sine);
		z2 = (int)Math.round(8.0*sine - 10.0*cos);
		x3 = (int)Math.round(25.0*cos);
		z3 = (int)Math.round(25.0*sine);
		
		fill(0);
		ellipse(xC,zC,25,25);
		triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	
	public void shadowPrint(int xC, int zC,double rT,int color)
	{
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(rT);
		double sine = Math.sin(rT);		
		
		x1 = (int)Math.round(4.0*cos - 5.0*sine);
		z1 = (int)Math.round(4.0*sine + 5.0*cos);
		x2 = (int)Math.round(4.0*cos + 5.0*sine);
		z2 = (int)Math.round(4.0*sine - 5.0*cos);
		x3 = (int)Math.round(12.5*cos);
		z3 = (int)Math.round(12.5*sine);
		
		fill(color);
		ellipse(xC,zC,13,13);
		triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	public void shadow()
	{
		int length = trace.size();
		for (int i = 0; i < length; i+=3) {
			int xC = (int)Math.round((double)trace.get(i));
			int zC = (int)Math.round((double)trace.get(i+1));
			double or = (double)trace.get(i+2);
			shadowPrint(xC,zC,or,180*(length-i)/length);
		}
	}
	
	public void setNewOrientation()
	{
		double r2, rot=0;
		if((vz*vz + vx*vx) > 0)
		{
			r2 = Math.atan2(vz,vx);
			rot = r2 - r;
			if(Math.abs(rot)<rMax)
			{
				r = r2;
				rot = 0;
			}
			else
			{
				if(rot > pi)
					rot -= 2*pi;
				else if(rot < -pi)
					rot += 2*pi;
				
				if( Math.abs(rot) > rMax)
					r =  (rot > 0)? rMax+r : (-rMax+r);
			}
		}
	}
	
	public boolean seek(double targetX, double targetZ)
	{
		boolean targetReached = false;
		double mvr = Math.sqrt(vMax);
		
		double velX = targetX - x;
		double velZ = targetZ - z; 
		
		if(Math.abs(velX) <= 2 && Math.abs(velZ) <= 2 )
		{
			vx = vz = 0;
			targetReached = true;
		}
		else
		{
			if(velX == 0 && velZ == 0)
				vz = vx = 0;
			else
			{
				vx = (velX/Math.sqrt(velX*velX+velZ*velZ))*mvr;
				vz = (velZ/Math.sqrt(velX*velX+velZ*velZ))*mvr;
			}
		}
		setNewOrientation();
		return targetReached;
	}
	public void draw() {
		  noStroke();
		  background(255);
		  
		  shadow();
		  shapePrint();
		  
		 update();
	}
	
	public void update()
	{
		long newTime = System.currentTimeMillis();
		long diff = newTime - time;
		
		if(seek(tList[target][0],tList[target][1]))
		{
			target = (target+1)%4;
		}
		
		x += vx*diff;
		z += vz*diff;
		
		if(counter == 0)
		{
			if(trace.size() > traceLength)
			{
				trace.remove(0);
				trace.remove(0);
				trace.remove(0);
			}
			trace.add(x);
			trace.add(z);
			trace.add(r);
		}
		counter = (counter+1) % 20;
		time = newTime;
	}	
	public static void main(String _args[]) {
		PApplet.main(new String[] { assignment1.Assignment1.class.getName() });
	}
}



