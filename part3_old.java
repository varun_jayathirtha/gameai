package assignment1;

import processing.core.PApplet;
import java.util.*;

public class part3 extends PApplet {
	int xMax, zMax;
	
	// Kinematic 
	double x, z;
	double vx, vz, vr;
	double r;
	
	double vMax,vMaxSq, rMax, arMax;
	// steering output
	double ax, az;
	double ar;
	
	final double pi = Math.PI;
	
	double targetX, targetZ;
	boolean started;
	
	int counter,target;
	int traceLength;
	long time;
	
	List trace = new ArrayList();
	  
	public void setup() {
	}
	public void settings() {
		started = false;
		targetX = 0;
		targetZ = 0;
		xMax = 700;
		zMax = 700;
		x = 275;
		z = 275;
		vx = 0.00;
		vz = 0.00;
		vr = 0.00;
		r = 0;
		
		vMax = 0.02;
		vMaxSq = Math.sqrt(vMax);
		rMax = 0.9;
		arMax = 0.01;
//									rMax = 0.5;
//									arMax = 0.01;
		counter = 0;
		traceLength = 50;
		time = System.currentTimeMillis();
	    size(xMax,zMax);
	}
	public void shapePrint()
	{
		int xC, zC;
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(r);
		double sine = Math.sin(r);		
				
		xC = (int)x;
		zC = (int)z;
		x1 = (int)Math.round(8.0*cos - 10.0*sine);
		z1 = (int)Math.round(8.0*sine + 10.0*cos);
		x2 = (int)Math.round(8.0*cos + 10.0*sine);
		z2 = (int)Math.round(8.0*sine - 10.0*cos);
		x3 = (int)Math.round(25.0*cos);
		z3 = (int)Math.round(25.0*sine);
		
		fill(0);
		ellipse(xC,zC,25,25);
		triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	public void shadowPrint(int xC, int zC,double rT,int color)
	{
		int x1,x2,x3,z1,z2,z3;
		double cos = Math.cos(rT);
		double sine = Math.sin(rT);		
		
		x1 = (int)Math.round(4.0*cos - 5.0*sine);
		z1 = (int)Math.round(4.0*sine + 5.0*cos);
		x2 = (int)Math.round(4.0*cos + 5.0*sine);
		z2 = (int)Math.round(4.0*sine - 5.0*cos);
		x3 = (int)Math.round(12.5*cos);
		z3 = (int)Math.round(12.5*sine);
		
		fill(color);
		ellipse(xC,zC,13,13);
		triangle(xC+x1,zC+z1,xC+x2,zC+z2,xC+x3,zC+z3);
	}
	public void shadow()
	{
		int length = trace.size();
		for (int i = 0; i < length; i+=3) {
			int xC = (int)Math.round((double)trace.get(i));
			int zC = (int)Math.round((double)trace.get(i+1));
			double or = (double)trace.get(i+2);
			shadowPrint(xC,zC,or,180*(length-i)/length);
		}
	}
	
	public void setNewOrientation2()
	{
		double r2, rot=0;
		if((vz*vz + vx*vx) > 0)
		{
			r2 = Math.atan2(vz,vx);
			rot = r2 - r;
			
			if(rot > pi)
				rot -= 2*pi;
			else if(rot < -pi)
				rot += 2*pi;
			
			if(Math.abs(rot)<rMax)
			{
				r = r2;
				rot = 0;
			}
			else
			{
				if( Math.abs(rot) > rMax)
					rot =  (rot > 0)? rMax : -rMax;
				r += rot;
			}
		}
	}
	
	public void setNewOrientation()
	{
		double rand = Math.random() - Math.random() + Math.random() - Math.random() ;
		//vr = (Math.random() * rMax * 2) - rMax;
		vr = rand * rMax / 2;
		r += vr;
		r = r % (2*pi);
	}
	
	/*
	public void setNewOrientation()
	{
		double newTime = System.currentTimeMillis();
		double diff = System.currentTimeMillis() - time;
		
		ar = (Math.random() * arMax * 2) - arMax;
		vr += ar*diff;
		r = (r + vr*diff ) % (2*pi);
		System.out.println(r+" : "+vr+" : "+ar+" : "+diff);
	}
	*/
	
	// Gradual Stop
	
	public void seek()
	{
		boolean entered = false;
		double entryAngle = 0;
		x += 10 * vMaxSq * Math.cos(r);
		z += 10 * vMaxSq * Math.sin(r);
		
		if(x > 650) x = 650;
		if(x < 50) x = 50;
		if(z > 650) z = 650;
		if(z < 50) z = 50;
		
		if((r <= 2*pi) && (x >= 650 || x <= 50 || z >= 650 || z <= 50) )
		{
			r += pi/16;
		}
		
	}
	
	
	public void draw() {
		  noStroke();
		  background(255);
		  
		  shadow();
		  shapePrint();
		  
		 update();
	}
	
	public void update()
	{
		long newTime = System.currentTimeMillis();
		
		if(counter%8 == 0)
			setNewOrientation();
		seek();
		
//		if(newTime - time >= 150)
//		{
//			setNewOrientation();
//			time = newTime;
//		}
		
		setNewOrientation2();
		
		if(counter == 0 )
		{
			if(trace.size() > traceLength)
			{
				trace.remove(0);
				trace.remove(0);
				trace.remove(0);
			}
			trace.add(x);
			trace.add(z);
			trace.add(r);
			
		}
		counter = (counter+1) % 30;
	}	
	
	public static void main(String _args[]) {
		PApplet.main(new String[] { assignment1.part3.class.getName() });
	}
}


